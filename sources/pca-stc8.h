/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __PCA_STC8_H
#define __PCA_STC8_H

/**
 * @file pca-stc8.h
 * 
 * PCA abstraction layer: STC8-specific definitions.
 * 
 * **IMPORTANT:** In order to satisfy SDCC's requirements for ISR 
 * handling, this header file **MUST** be included in the C source 
 * file where main() is defined.
 * 
 * The STC8 provides 2 distinct sets of PWM channels: those from
 * the PCA, manipulated with functions in ccp.h and associated with
 * CCP<n> pins, and the Enhanced PWM Channels, which are completely
 * independent from the PCA and are associated with PWM<n> pins.
 */

typedef enum {
	PCA_SYSCLK_DIV12 = 0,
	PCA_SYSCLK_DIV2 = 1,
	PCA_TIMER0 = 2,
	PCA_ECI = 3,
	PCA_SYSCLK = 4,
	PCA_SYSCLK_DIV4 = 5,
	PCA_SYSCLK_DIV6 = 6,
	PCA_SYSCLK_DIV8 = 7,
} PCA_ClockSource;

typedef enum {
	PCA_FREE_RUNNING = 0,
	PCA_ECI_ENABLED = 1,
} PCA_CounterMode;

typedef enum {
	PCA_CONTINUOUS = 0,
	PCA_ONE_SHOT = 1,
} PCA_CaptureMode;

typedef enum {
	PCA_INTERRUPT_DISABLE = 0,
	PCA_INTERRUPT_ENABLE = 1,
} PCA_Interrupt;

typedef enum {
	PCA_RISING_EDGE = 2,
	PCA_FALLING_EDGE = 1,
	PCA_BOTH_EDGES = 3,
} PCA_EdgeTrigger;

typedef enum {
	PCA_CHANNEL0 = 0,
	PCA_CHANNEL1 = 1,
	PCA_CHANNEL2 = 2,
	PCA_CHANNEL3 = 3,
} PCA_Channel;

typedef enum {
	PCA_P1 = 0,	/*!< P1.7 = CCP0, P1.6 = CCP1, P1.5 = CCP2, P1.4 = CCP3, P1.2 = ECI */
	PCA_P2 = 1,	/*!< P2.3 = CCP0, P2.4 = CCP2, P2.5 = CCP2, P2.6 = CCP3, P2.2 = ECI */
	PCA_P7 = 2,	/*!< P7.0 = CCP0, P7.1 = CCP7, P7.2 = CCP2, P7.3 = CCP3, P7.4 = ECI */
	PCA_P3 = 3,	/*!< P3.3 = CCP0, P3.2 = CCP3, P3.1 = CCP2, P3.0 = CCP3, P3.5 = ECI */
} PCA_Port;

typedef enum {
	PCA_8BIT_PWM = 0,
	PCA_7BIT_PWM = 1,
	PCA_6BIT_PWM = 2,
	PCA_10BIT_PWM = 3,
} PCA_PWM_Bits;

#include "gpio.h"

/**
 * GPIO pin configurations for PCA channels (convenience macros).
 */

#define PCA_P1_CHANNEL0_PIN		GPIO_PIN_CONFIG(GPIO_PORT1, GPIO_PIN7, GPIO_BIDIRECTIONAL)
#define PCA_P1_CHANNEL1_PIN		GPIO_PIN_CONFIG(GPIO_PORT1, GPIO_PIN6, GPIO_BIDIRECTIONAL)
#define PCA_P1_CHANNEL2_PIN		GPIO_PIN_CONFIG(GPIO_PORT1, GPIO_PIN5, GPIO_BIDIRECTIONAL)
#define PCA_P1_CHANNEL3_PIN		GPIO_PIN_CONFIG(GPIO_PORT1, GPIO_PIN4, GPIO_BIDIRECTIONAL)
#define PCA_P1_ECI_PIN			GPIO_PIN_CONFIG(GPIO_PORT1, GPIO_PIN2, GPIO_BIDIRECTIONAL)

#define PCA_P2_CHANNEL0_PIN		GPIO_PIN_CONFIG(GPIO_PORT2, GPIO_PIN3, GPIO_BIDIRECTIONAL)
#define PCA_P2_CHANNEL1_PIN		GPIO_PIN_CONFIG(GPIO_PORT2, GPIO_PIN4, GPIO_BIDIRECTIONAL)
#define PCA_P2_CHANNEL2_PIN		GPIO_PIN_CONFIG(GPIO_PORT2, GPIO_PIN5, GPIO_BIDIRECTIONAL)
#define PCA_P2_CHANNEL3_PIN		GPIO_PIN_CONFIG(GPIO_PORT2, GPIO_PIN6, GPIO_BIDIRECTIONAL)
#define PCA_P2_ECI_PIN			GPIO_PIN_CONFIG(GPIO_PORT2, GPIO_PIN2, GPIO_BIDIRECTIONAL)

#define PCA_P3_CHANNEL0_PIN		GPIO_PIN_CONFIG(GPIO_PORT3, GPIO_PIN3, GPIO_BIDIRECTIONAL)
#define PCA_P3_CHANNEL1_PIN		GPIO_PIN_CONFIG(GPIO_PORT3, GPIO_PIN2, GPIO_BIDIRECTIONAL)
#define PCA_P3_CHANNEL2_PIN		GPIO_PIN_CONFIG(GPIO_PORT3, GPIO_PIN1, GPIO_BIDIRECTIONAL)
#define PCA_P3_CHANNEL3_PIN		GPIO_PIN_CONFIG(GPIO_PORT3, GPIO_PIN0, GPIO_BIDIRECTIONAL)
#define PCA_P3_ECI_PIN			GPIO_PIN_CONFIG(GPIO_PORT3, GPIO_PIN5, GPIO_BIDIRECTIONAL)

#define PCA_P7_CHANNEL0_PIN		GPIO_PIN_CONFIG(GPIO_PORT7, GPIO_PIN0, GPIO_BIDIRECTIONAL)
#define PCA_P7_CHANNEL1_PIN		GPIO_PIN_CONFIG(GPIO_PORT7, GPIO_PIN1, GPIO_BIDIRECTIONAL)
#define PCA_P7_CHANNEL2_PIN		GPIO_PIN_CONFIG(GPIO_PORT7, GPIO_PIN2, GPIO_BIDIRECTIONAL)
#define PCA_P7_CHANNEL3_PIN		GPIO_PIN_CONFIG(GPIO_PORT7, GPIO_PIN3, GPIO_BIDIRECTIONAL)
#define PCA_P7_ECI_PIN			GPIO_PIN_CONFIG(GPIO_PORT7, GPIO_PIN4, GPIO_BIDIRECTIONAL)

void __pca_isr() __interrupt 7 __using 1;

#define __PCA_PLATFORM_H
#include "pca.h"

#endif // __PCA_STC8_H
