/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "lcd-text.h"
#include "lcd-controller.h"
#include <stdio.h>
#include <string.h>

/**
 * @file lcd-text.c
 * 
 * Text operations for LCD devices: implementation.
 */

void lcd_txtInitialiseDisplayMode(LCD_Device *device) {
	lcd_disableGraphicsDisplay(device);
	lcd_txtClear(device);
}

void lcd_txtClear(LCD_Device *device) {
	lcd_clearTextDisplay(device);
}

void lcd_txtPrintAt(LCD_Device *device, unsigned int row, unsigned int column, const char *__fmt, ...) {
    va_list vaList;
    char buffer[81];

	// Strings of latin characters can start at any position provided
	// their length is an even number.
	// 16-bit Chinese characters can only start at an even position.
	// For the sake of simplicity, we do not want to start at odd columns.
	column &= ~1;
	lcd_setTextDisplayPosition(device, row, column);

    // CAUTION! SDCC doesn't provide vsnprintf(), so beware of buffer 
    // overflows if you can print arbitrary long values.
    va_start(vaList, __fmt);
    vsprintf(buffer, __fmt, vaList);
    va_end(vaList);
    
    // Make sure the length of the string is an even number
    // by adding a space if it isn't.
    unsigned int l = strlen(buffer);
    
	if (l & 1) {
		buffer[l++] = ' ';
		buffer[l] = '\0';
	}
	
	// Truncate the string if it would go past the end of the row.
    unsigned int lMax = device->textWidth - column;
    lMax &= ~1;
    
    if (l > lMax) {
		buffer[lMax] = '\0';
	}

	// Now we're good to go!
    for (char *c = buffer; *c; c++) {
		lcd_writeByte(device, *c);
    }
}
