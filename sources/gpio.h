/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _GPIO_H
#define _GPIO_H

/**
 * @file gpio.h
 * 
 * GPIO abstraction layer.
 */

typedef enum {
	GPIO_BIDIRECTIONAL = 0,
	GPIO_PUSH_PULL = 1,
	GPIO_HIGH_IMPEDANCE = 2,
	GPIO_OPEN_DRAIN = 3,
} GpioMode;

typedef enum {
	GPIO_DISABLED = 0,
	GPIO_ENABLED = 1,
} GpioBoolean;

typedef enum {
	GPIO_PORT0 = 0,
	GPIO_PORT1,
	GPIO_PORT2,
	GPIO_PORT3,
	GPIO_PORT4,
	GPIO_PORT5,
	GPIO_PORT6,
	GPIO_PORT7,
} GpioPort;

typedef enum {
	GPIO_PIN0 = 0,
	GPIO_PIN1,
	GPIO_PIN2,
	GPIO_PIN3,
	GPIO_PIN4,
	GPIO_PIN5,
	GPIO_PIN6,
	GPIO_PIN7,
} GpioPin;

/**
 * Configuration details of a GPIO pin, or series of consecutive pins.
 * 
 * **IMPORTANT:** symbols whose names beginning with 2 underscores are 
 * used internally by the driver.
 */
typedef struct {
	GpioPort port;			/*!< GPIO port number. */
	GpioPin pin;			/*!< Index of the first pin used on the port. */
	unsigned char count;	/*!< Number of consecutive pins used on the port, 
							starting with .pin and counting up. */
	GpioMode mode;			/*!< GPIO pin mode of operation. */
	GpioBoolean schmidtTrigger;	/*!< Schmidt trigger activation. */
	GpioBoolean internalPullUp;	/*!< Internal pull-up resistor activation. */
	
	unsigned char __setMask;
	unsigned char __clearMask;
} GpioConfig;

/**
 * Configuration status codes.
 */
typedef enum {
	GPIO_OK = 0,			/*!< Configuration succeeded. */
	GPIO_PortNotAvailable,	/*!< The requested port is not available on this specific MCU model. */
} Gpio_StatusCode;

/**
 * Convenience macro to configure an entire port.
 */
#define GPIO_PORT_CONFIG(gpioPort, gpioMode) { .port = gpioPort, .pin = 0, .count = 8, .mode = gpioMode, .schmidtTrigger = GPIO_DISABLED, .internalPullUp = GPIO_DISABLED }

/**
 * Convenience macro to configure a single GPIO pin.
 */
#define GPIO_PIN_CONFIG(gpioPort, gpioPin, gpioMode) { .port = gpioPort, .pin = gpioPin, .count = 1, .mode = gpioMode, .schmidtTrigger = GPIO_DISABLED, .internalPullUp = GPIO_DISABLED }

/**
 * Configures a GPIO pin, or series of consecutive pins.
 */
Gpio_StatusCode gpio_configure(GpioConfig *config);

/**
 * Reads a GPIO pin, or series of consecutive pins.
 * 
 * When reading a series of pins, the resulting value will be shifted
 * so that bit 0 of the result corresponds to .pin in GpioConfig.
 * Bits outside of the scope defined by GpioConfig are masked off.
 */
unsigned char gpio_read(GpioConfig *config);

/**
 * Sets a GPIO pin, or series of consecutive pins.
 * 
 * When setting a series of pins, 'value' will be shifted so that its 
 * bit 0 corresponds to .pin in GpioConfig.
 * Bits outside of the scope defined by GpioConfig are masked off.
 */
void gpio_write(GpioConfig *config, unsigned char value);

#endif // _GPIO_H
