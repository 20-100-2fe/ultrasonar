/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __PCA_H
#define __PCA_H

#ifndef __PCA_PLATFORM_H
#error "File pca-<your MCU>.h MUST be included BEFORE pca.h"
#endif

/**
 * @file pca.h
 * 
 * PCA abstraction layer: MCU-independent definitions.
 */

// Compare / Capture / PWM module: 
// ---------------------------------------------------------------------

/**
 * Initialises the master counter of the counter array.
 * 
 * For source code clarity, should be called before any other PCA_* function.
 * 
 * PCA = Programmable Counter Array
 * 
 * CCP = Compare / Capture / PWM
 */
void pca_initialise(PCA_ClockSource clockSource, PCA_CounterMode counterMode, PCA_Interrupt overflowInterrupt, PCA_Port port);

/**
 * Helper function to configure and start Timer0 to achieve a given frequency.
 * 
 * Must be used when clockSource == PCA_TIMER0.
 * @returns -1 if the requested frequency is too HIGH to be obtained,
 * -2 if the requested frequency is too LOW to be obtained,
 * or 0 if the timer was successfully configured and started.
 */
int pca_startTimer0(unsigned long frequency);

/**
 * Configures a PCA channel to measure the width of a pulse.
 */
void pca_startCapture(PCA_Channel channel, PCA_EdgeTrigger trigger, PCA_CaptureMode captureMode);

/**
 * Configures a PCA channel in PWM mode.
 * 
 * The duty cycle is defined by the number of clockSource pulses during 
 * which the PWM output must be high.
 */
void pca_startPwm(PCA_Channel channel, PCA_PWM_Bits bits, unsigned int clocksHigh);

/**
 * Changes the duty cycle of a PWM channel.
 * 
 * All other configuration parameters remain unchanged.
 */
void pca_setPwmDutyCycle(PCA_Channel channel, unsigned int clocksHigh);

/**
 * MUST be implemented by the application, even when not used.
 * 
 * **IMPORTANT** When used, must keep track of the measured pulse 
 * length, set a flag to indicate the measure is available, and 
 * **NOTHING** else!
 */
void pca_onCapture(PCA_Channel channel, unsigned long pulseLength) __using 1;

#endif // __PCA_H
