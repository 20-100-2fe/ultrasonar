/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "stc8f.h"
#include "pca-stc8.h"

/**
 * @file pca-stc8.c
 * 
 * PCA abstraction layer: implementation for the STC8.
 */

#define PCA_CHANNEL_COUNT 4

typedef union {
	struct {
		unsigned char ccapl;
		unsigned char ccaph;
		unsigned char cnt;
		unsigned char zero;
	} fields;
	unsigned long count;
} PCA_CaptureCount;

PCA_CaptureCount __pca_captureStartCount[PCA_CHANNEL_COUNT];
PCA_CaptureCount __pca_captureEndCount[PCA_CHANNEL_COUNT];
PCA_CaptureMode __pca_captureMode[PCA_CHANNEL_COUNT];
unsigned char __pca_captureOverflowCount[PCA_CHANNEL_COUNT];
unsigned char __pca_captureActiveChannels;

void __pca_isr() __interrupt 7 __using 1 {
	__critical {
		unsigned char ccapl = 0;
		unsigned char ccaph = 0;
		unsigned char channel = PCA_CHANNEL_COUNT;
		
		if (CF) {
			CF = 0;
			
			if (__pca_captureActiveChannels) {
				__pca_captureOverflowCount[PCA_CHANNEL0]++;
				__pca_captureOverflowCount[PCA_CHANNEL1]++;
				__pca_captureOverflowCount[PCA_CHANNEL2]++;
				__pca_captureOverflowCount[PCA_CHANNEL3]++;
			}
		}
		
		if (CCF0) {
			CCF0 = 0;
			channel = PCA_CHANNEL0;
			ccapl = CCAP0L;
			ccaph = CCAP0H;
			
			if (__pca_captureMode[channel] == PCA_ONE_SHOT) {
				CCAPM0 = 0;
				__pca_captureActiveChannels--;
			}
		}
		
		if (CCF1) {
			CCF1 = 0;
			channel = PCA_CHANNEL1;
			ccapl = CCAP1L;
			ccaph = CCAP1H;
			
			if (__pca_captureMode[channel] == PCA_ONE_SHOT) {
				CCAPM1 = 0;
				__pca_captureActiveChannels--;
			}
		}
		
		if (CCF2) {
			CCF2 = 0;
			channel = PCA_CHANNEL2;
			ccapl = CCAP2L;
			ccaph = CCAP2H;
			
			if (__pca_captureMode[channel] == PCA_ONE_SHOT) {
				CCAPM2 = 0;
				__pca_captureActiveChannels--;
			}
		}
		
		if (CCF3) {
			CCF3 = 0;
			channel = PCA_CHANNEL3;
			ccapl = CCAP3L;
			ccaph = CCAP3H;
			
			if (__pca_captureMode[channel] == PCA_ONE_SHOT) {
				CCAPM3 = 0;
				__pca_captureActiveChannels--;
			}
		}
		
		if (channel < PCA_CHANNEL_COUNT) {
			__pca_captureStartCount[channel].count = __pca_captureEndCount[channel].count;
			__pca_captureEndCount[channel].fields.ccapl = ccapl;
			__pca_captureEndCount[channel].fields.ccaph = ccaph;
			__pca_captureEndCount[channel].fields.cnt = __pca_captureOverflowCount[channel];
			__pca_captureEndCount[channel].fields.zero = 0;
			pca_onCapture(channel, __pca_captureEndCount[channel].count - __pca_captureStartCount[channel].count);
		}
	}
}

void pca_initialise(PCA_ClockSource clockSource, PCA_CounterMode counterMode, PCA_Interrupt overflowInterrupt, PCA_Port port) {
	// All CCP channels are assigned to the same port (default is P1).
	// CCP_P1 => ECI = P1.2 - CCP0 = P1.7 - CCP1 = P1.6 - CCP2 = P1.5 - CCP3 = P1.4
	// CCP_P2 => ECI = P2.2 - CCP0 = P2.3 - CCP1 = P2.4 - CCP2 = P2.5 - CCP3 = P2.6
	// CCP_P7 => ECI = P7.4 - CCP0 = P7.0 - CCP1 = P7.1 - CCP2 = P7.2 - CCP3 = P7.3
	// CCP_P3 => ECI = P3.5 - CCP0 = P3.3 - CCP1 = P3.2 - CCP2 = P3.1 - CCP3 = P3.0
	P_SW1 = (P_SW1 & 0xcf) | (port << 4);
	
	__pca_captureActiveChannels = 0;
	
	CMOD = (counterMode << 7) | (clockSource << 1) | overflowInterrupt;
	CCON = 0;
	CL = 0x00;
	CH = 0x00;
	CR = 1;
}

int pca_startTimer0(unsigned long frequency) {
	int rc = 0;
	unsigned long divisor = F_CPU / frequency;
	unsigned char timerMode = 0;
	unsigned char sysclkDiv1 = 1;
	
	if (divisor == 0) {
		// frequency too high
		rc = -1;
	} else if (divisor > 786432UL) {
		// frequency too low (786432 = 65536 * 12)
		rc = -2;
	} else {
		if (divisor > 65536UL || (divisor > 256UL && divisor <= 3072UL && (divisor % 12) == 0)) {
			// sysclkDiv1 = 0 => pre-divide sysclk by 12
			sysclkDiv1 = 0;
			divisor /= 12UL;
		}
		
		unsigned int reloadValue;
		
		if (divisor <= 256UL) {
			timerMode = 2;
			reloadValue = (unsigned int) (256UL - divisor);
		} else {
			reloadValue = (unsigned int) (65536UL - divisor);
		}
		
		// Set prescaler
		AUXR = (AUXR & 0x7f) | (sysclkDiv1 << 7);
		// Set timer mode
		TMOD = (TMOD & 0xf0) | timerMode;
		
		if (timerMode == 0) {
			// Mode 0 = 16-bit auto-reload
			TL0 = reloadValue & 0xff;
			TH0 = reloadValue >> 8;
		} else {
			// Mode 2 = 8-bit auto-reload
			TH0 = TL0 = reloadValue;
		}
		
		// Clear flags and start timer
		TCON = (TCON & 0xcc) | 0x10;
	}
	
	return rc;
}

void pca_startCapture(PCA_Channel channel, PCA_EdgeTrigger trigger, PCA_CaptureMode captureMode) {
	__pca_captureStartCount[channel].count = 0;
	__pca_captureEndCount[channel].count = 0;
	__pca_captureOverflowCount[channel] = 0;
	__pca_captureMode[channel] = captureMode;
	__pca_captureActiveChannels++;
	
	CR = 0;
	CL = 0;
	CH = 0;
	
	switch (channel) {
	case PCA_CHANNEL0:
		CCAPM0 = (trigger << 4) | PCA_INTERRUPT_ENABLE;
		CCAP0L = 0;
		CCAP0H = 0;
		break;
	case PCA_CHANNEL1:
		CCAPM1 = (trigger << 4) | PCA_INTERRUPT_ENABLE;
		CCAP1L = 0;
		CCAP1H = 0;
		break;
	case PCA_CHANNEL2:
		CCAPM2 = (trigger << 4) | PCA_INTERRUPT_ENABLE;
		CCAP2L = 0;
		CCAP2H = 0;
		break;
	case PCA_CHANNEL3:
		CCAPM3 = (trigger << 4) | PCA_INTERRUPT_ENABLE;
		CCAP3L = 0;
		CCAP3H = 0;
		break;
	}
	
	CR = 1;
}

void __pca_configurePWM(unsigned char initialise, PCA_Channel channel, PCA_PWM_Bits pwmBits, unsigned int clocksHigh) {
	PCA_PWM_Bits bits = pwmBits;
	
	if (!initialise) {
		switch (channel) {
		case PCA_CHANNEL0:
			bits = (PCA_PWM_Bits) ((PCA_PWM0 >> 6) & 3);
			break;
		case PCA_CHANNEL1:
			bits = (PCA_PWM_Bits) ((PCA_PWM1 >> 6) & 3);
			break;
		case PCA_CHANNEL2:
			bits = (PCA_PWM_Bits) ((PCA_PWM2 >> 6) & 3);
			break;
		case PCA_CHANNEL3:
			bits = (PCA_PWM_Bits) ((PCA_PWM3 >> 6) & 3);
			break;
		}
	}
	
	unsigned int maxValue = 0;
	
	switch (bits) {
	case PCA_8BIT_PWM:
		maxValue = 256;
		break;
	case PCA_7BIT_PWM:
		maxValue = 128;
		break;
	case PCA_6BIT_PWM:
		maxValue = 64;
		break;
	case PCA_10BIT_PWM:
		maxValue = 1024;
		break;
	}
	
	unsigned int reloadValue = maxValue - clocksHigh;
	unsigned char xccap = (reloadValue >> 8) & 3;
	unsigned char ccap = reloadValue & 0xff;
	unsigned char pcaPwm = (bits << 6) | (xccap << 4) | (xccap << 2);
	
	switch (channel) {
	case PCA_CHANNEL0:
		PCA_PWM0 = pcaPwm;
		
		if (initialise) {
			CCAPM0 = 0x42;
			CCAP0L = ccap;
		}
		
		CCAP0H = ccap;
		break;
	case PCA_CHANNEL1:
		PCA_PWM1 = pcaPwm;
		
		if (initialise) {
			CCAPM1 = 0x42;
			CCAP1L = ccap;
		}
		
		CCAP1H = ccap;
		break;
	case PCA_CHANNEL2:
		PCA_PWM2 = pcaPwm;
		
		if (initialise) {
			CCAPM2 = 0x42;
			CCAP2L = ccap;
		}
		
		CCAP2H = ccap;
		break;
	case PCA_CHANNEL3:
		PCA_PWM3 = pcaPwm;
		
		if (initialise) {
			CCAPM3 = 0x42;
			CCAP3L = ccap;
		}
		
		CCAP3H = ccap;
		break;
	}
}

void pca_startPwm(PCA_Channel channel, PCA_PWM_Bits bits, unsigned int clocksHigh) {
	__pca_configurePWM(1, channel, bits, clocksHigh);
}

void pca_setPwmDutyCycle(PCA_Channel channel, unsigned int clocksHigh) {
	__pca_configurePWM(0, channel, 0, clocksHigh);
}
