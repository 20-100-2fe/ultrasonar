/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "stc8f.h"
#include "gpio.h"

/**
 * @file gpio-stc8.c
 * 
 * STC8 implementation of the GPIO abstraction layer.
 */

inline unsigned char __gpio_setBits(unsigned char portValue, unsigned char cfgValue, GpioConfig *gpioConfig) {
	return cfgValue ? (portValue | gpioConfig->__setMask) : (portValue & gpioConfig->__clearMask);
}

Gpio_StatusCode gpio_configure(GpioConfig *gpioConfig) {
	Gpio_StatusCode result = GPIO_OK;
	
	// Pre-generate bit masks for read/write operations
	unsigned char mask = 0;
	
	for (unsigned char n = gpioConfig->count; n > 0; n--) {
		mask = mask << 1;
		mask |= 1;
	}
	
	gpioConfig->__setMask = mask << gpioConfig->pin;
	gpioConfig->__clearMask = ~gpioConfig->__setMask;
	
	switch (gpioConfig->port) {
	case GPIO_PORT0:
		P0M1 = __gpio_setBits(P0M1, gpioConfig->mode & 2, gpioConfig);
		P0M0 = __gpio_setBits(P0M0, gpioConfig->mode & 1, gpioConfig);
		P0PU = __gpio_setBits(P0PU, gpioConfig->internalPullUp, gpioConfig);
		P0NCS = __gpio_setBits(P0NCS, gpioConfig->schmidtTrigger, gpioConfig);
		break;
	
	case GPIO_PORT1:
		P1M1 = __gpio_setBits(P1M1, gpioConfig->mode & 2, gpioConfig);
		P1M0 = __gpio_setBits(P1M0, gpioConfig->mode & 1, gpioConfig);
		P1PU = __gpio_setBits(P1PU, gpioConfig->internalPullUp, gpioConfig);
		P1NCS = __gpio_setBits(P1NCS, gpioConfig->schmidtTrigger, gpioConfig);
		break;
	
	case GPIO_PORT2:
		P2M1 = __gpio_setBits(P2M1, gpioConfig->mode & 2, gpioConfig);
		P2M0 = __gpio_setBits(P2M0, gpioConfig->mode & 1, gpioConfig);
		P2PU = __gpio_setBits(P2PU, gpioConfig->internalPullUp, gpioConfig);
		P2NCS = __gpio_setBits(P2NCS, gpioConfig->schmidtTrigger, gpioConfig);
		break;
	
	case GPIO_PORT3:
		P3M1 = __gpio_setBits(P3M1, gpioConfig->mode & 2, gpioConfig);
		P3M0 = __gpio_setBits(P3M0, gpioConfig->mode & 1, gpioConfig);
		P3PU = __gpio_setBits(P3PU, gpioConfig->internalPullUp, gpioConfig);
		P3NCS = __gpio_setBits(P3NCS, gpioConfig->schmidtTrigger, gpioConfig);
		break;
	
	case GPIO_PORT4:
		P4M1 = __gpio_setBits(P4M1, gpioConfig->mode & 2, gpioConfig);
		P4M0 = __gpio_setBits(P4M0, gpioConfig->mode & 1, gpioConfig);
		P4PU = __gpio_setBits(P4PU, gpioConfig->internalPullUp, gpioConfig);
		P4NCS = __gpio_setBits(P4NCS, gpioConfig->schmidtTrigger, gpioConfig);
		break;
	
	case GPIO_PORT5:
		P5M1 = __gpio_setBits(P5M1, gpioConfig->mode & 2, gpioConfig);
		P5M0 = __gpio_setBits(P5M0, gpioConfig->mode & 1, gpioConfig);
		P5PU = __gpio_setBits(P5PU, gpioConfig->internalPullUp, gpioConfig);
		P5NCS = __gpio_setBits(P5NCS, gpioConfig->schmidtTrigger, gpioConfig);
		break;
	
	case GPIO_PORT6:
		P6M1 = __gpio_setBits(P6M1, gpioConfig->mode & 2, gpioConfig);
		P6M0 = __gpio_setBits(P6M0, gpioConfig->mode & 1, gpioConfig);
		P6PU = __gpio_setBits(P6PU, gpioConfig->internalPullUp, gpioConfig);
		P6NCS = __gpio_setBits(P6NCS, gpioConfig->schmidtTrigger, gpioConfig);
		break;
	
	case GPIO_PORT7:
		P7M1 = __gpio_setBits(P7M1, gpioConfig->mode & 2, gpioConfig);
		P7M0 = __gpio_setBits(P7M0, gpioConfig->mode & 1, gpioConfig);
		P7PU = __gpio_setBits(P7PU, gpioConfig->internalPullUp, gpioConfig);
		P7NCS = __gpio_setBits(P7NCS, gpioConfig->schmidtTrigger, gpioConfig);
		break;
	}
	
	return result;
}

unsigned char gpio_read(GpioConfig *gpioConfig) {
	unsigned char value = 0;
	
	switch (gpioConfig->port) {
	case GPIO_PORT0:
		value = P0;
		break;
	
	case GPIO_PORT1:
		value = P1;
		break;
	
	case GPIO_PORT2:
		value = P2;
		break;
	
	case GPIO_PORT3:
		value = P3;
		break;
	
	case GPIO_PORT4:
		value = P4;
		break;
	
	case GPIO_PORT5:
		value = P5;
		break;
	
	case GPIO_PORT6:
		value = P6;
		break;
	
	case GPIO_PORT7:
		value = P7;
		break;
	}
	
	return (value & gpioConfig->__setMask) >> gpioConfig->pin;
}

void gpio_write(GpioConfig *gpioConfig, unsigned char value) {
	if (gpioConfig->count == 1) {
		// In case the caller wants to set a single bit and expects
		// any non-zero value will be treated as a logical one (which
		// makes sense in C), let's make sure we do.
		value = value ? 1 : 0;
	}
	
	value = (value << gpioConfig->pin) & gpioConfig->__setMask;
	
	switch (gpioConfig->port) {
	case GPIO_PORT0:
		P0 = P0 & gpioConfig->__clearMask | value;
		break;
	
	case GPIO_PORT1:
		P1 = P1 & gpioConfig->__clearMask | value;
		break;
	
	case GPIO_PORT2:
		P2 = P2 & gpioConfig->__clearMask | value;
		break;
	
	case GPIO_PORT3:
		P3 = P3 & gpioConfig->__clearMask | value;
		break;
	
	case GPIO_PORT4:
		P4 = P4 & gpioConfig->__clearMask | value;
		break;
	
	case GPIO_PORT5:
		P5 = P5 & gpioConfig->__clearMask | value;
		break;
	
	case GPIO_PORT6:
		P6 = P6 & gpioConfig->__clearMask | value;
		break;
	
	case GPIO_PORT7:
		P7 = P7 & gpioConfig->__clearMask | value;
		break;
	}
}
