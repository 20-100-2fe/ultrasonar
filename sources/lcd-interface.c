/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "lcd-interface.h"
#include "delay.h"

/**
 * @file lcd-interface.c
 * 
 * LCD communication interface abstraction layer: implementation.
 */

typedef enum {
	LCD_Instruction = 0,
	LCD_Data = 1,
} LCD_DataType;

typedef enum {
	LCD_Write = 0,
	LCD_Read = 1,
} LCD_TransferOperation;

static void __lcd_parallelDataOut(LCD_Interface *interface, LCD_DataType dataType, unsigned char byteValue) {
	LCD_ParallelLinkConfig *config = (LCD_ParallelLinkConfig *) interface->linkConfig;
	gpio_write(&config->rsOutput, dataType);
	gpio_write(&config->rwOutput, LCD_Write);
	gpio_write(&config->enOutput, 1);

	if (config->dataBus.count == 8) {
		gpio_write(&config->dataBus, byteValue);
	} else {
		gpio_write(&config->dataBus, byteValue >> 4);

		if (interface->__controllerLinkConfigured) {
			gpio_write(&config->enOutput, 0);
			gpio_write(&config->enOutput, 1);
			gpio_write(&config->dataBus, byteValue & 0x0f);
        }
	}

	gpio_write(&config->enOutput, 0);
}

static unsigned char __lcd_parallelDataIn(LCD_Interface *interface, LCD_DataType dataType) {
	unsigned char result = 0;
	LCD_ParallelLinkConfig *config = (LCD_ParallelLinkConfig *) interface->linkConfig;
	gpio_write(&config->rsOutput, dataType);
	gpio_write(&config->rwOutput, LCD_Read);
	gpio_write(&config->enOutput, 1);
	result = gpio_read(&config->dataBus);

	if (config->dataBus.count != 8) {
		gpio_write(&config->enOutput, 0);
		gpio_write(&config->enOutput, 1);
		result = (result << 4) | gpio_read(&config->dataBus);
	}

	gpio_write(&config->enOutput, 0);

	return result;
}

static void __lcd_parallelInitialise(LCD_Interface *interface) {
	LCD_ParallelLinkConfig *config = (LCD_ParallelLinkConfig *) interface->linkConfig;
	gpio_configure(&config->dataBus);
	gpio_configure(&config->enOutput);
	gpio_configure(&config->rwOutput);
	gpio_configure(&config->rsOutput);
	
	gpio_write(&config->enOutput, 0);
	gpio_write(&config->rwOutput, 0);
	gpio_write(&config->rsOutput, 0);
}

static void __lcd_serialSendByte(LCD_SerialLinkConfig *config, unsigned char byteValue) {
	// Note: bytes are sent MSB first
	unsigned char byte = byteValue;
	
	for (unsigned char bit = 8; bit; bit --) {
		// SCLK must stay high for at least 200 ns
		gpio_write(&config->sclkOutput, 1);
		
		// Data bit must be sent at least 40 ns before SCLK goes low,
		// and remain stable at least 40 ns after SCLK went low
		gpio_write(&config->dataInOut, byte & 0x80);
		byte = byte << 1;
		
		// SCLK must stay low for at least 200 ns
		gpio_write(&config->sclkOutput, 0);
		
		// On an MCS-51 operating at 24 MHz, those timing requirements 
		// are easily satisfied, but it's a good idea to check they're 
		// still satisfied when porting to a faster MCU.
	}
}

static void __lcd_serialBeginCommunication(LCD_SerialLinkConfig *config, LCD_DataType dataType, LCD_TransferOperation operation) {
	gpio_write(&config->csOutput, 1);
	__lcd_serialSendByte(config, 0xf8 | (operation << 2) | (dataType << 1));
}

static void __lcd_serialEndCommunication(LCD_SerialLinkConfig *config) {
	gpio_write(&config->csOutput, 0);
	gpio_write(&config->sclkOutput, 0);
}

static void __lcd_serialDataOut(LCD_Interface *interface, LCD_DataType dataType, unsigned char byteValue) {
	LCD_SerialLinkConfig *config = (LCD_SerialLinkConfig *) interface->linkConfig;
	__lcd_serialBeginCommunication(config, dataType, LCD_Write);
	__lcd_serialSendByte(config, byteValue & 0xf0);
	__lcd_serialSendByte(config, byteValue << 4);
	__lcd_serialEndCommunication(config);
}

// Suppress warning "unreferenced function argument"
#pragma disable_warning 85
static unsigned char __lcd_serialDataIn(LCD_Interface *interface, LCD_DataType dataType) {
	/*
	 * Data sheet says read operations are NOT supported in serial mode, 
	 * even though the timing diagram shows the position of the RW bit 
	 * in the packet. I tested and confirm reads don't work.
	 */
	return 0;
}

static void __lcd_serialInitialise(LCD_Interface *interface) {
	LCD_SerialLinkConfig *config = (LCD_SerialLinkConfig *) interface->linkConfig;
	gpio_configure(&config->dataInOut);
	gpio_configure(&config->csOutput);
	gpio_configure(&config->sclkOutput);
	
	gpio_write(&config->dataInOut, 0);
	gpio_write(&config->csOutput, 0);
	gpio_write(&config->sclkOutput, 0);
}

void lcd_initialiseInterface(LCD_Interface *interface) {
	interface->__controllerLinkConfigured = 0;
	
	switch (interface->linkType) {
	case LCD_ParallelLink:
		__lcd_parallelInitialise(interface);
		break;
	
	case LCD_SerialLink:
		__lcd_serialInitialise(interface);
		break;
	}
}

void lcd_controllerLinkConfigurationBegins(LCD_Interface *interface) {
	interface->__controllerLinkConfigured = 0;
}

void lcd_controllerLinkConfigurationComplete(LCD_Interface *interface) {
	interface->__controllerLinkConfigured = 1;
}

unsigned char lcd_getDataLinkWidth(LCD_Interface *interface) {
	unsigned char result = 8;
	
	// From the client code's perspective, the data link is 8 bits wide, 
	// how they are physically (serial or parallel) sent doesn't matter. 
	// The only situation when it has an impact is when initialising an 
	// LCD device over a 4-bit parallel interface.
	
	switch (interface->linkType) {
	case LCD_ParallelLink:
		result = ((LCD_ParallelLinkConfig *) interface->linkConfig)->dataBus.count;
		break;
	}
	
	return result;
}

void lcd_sendCommand(LCD_Interface *interface, unsigned char command) {
	switch (interface->linkType) {
	case LCD_ParallelLink:
		__lcd_parallelDataOut(interface, LCD_Instruction, command);
		break;
	
	case LCD_SerialLink:
		__lcd_serialDataOut(interface, LCD_Instruction, command);
		break;
	}
}

void lcd_sendData(LCD_Interface *interface, unsigned char data) {
	switch (interface->linkType) {
	case LCD_ParallelLink:
		__lcd_parallelDataOut(interface, LCD_Data, data);
		break;
	
	case LCD_SerialLink:
		__lcd_serialDataOut(interface, LCD_Data, data);
		break;
	}
}

unsigned char lcd_readStatus(LCD_Interface *interface) {
	unsigned char result = 0;
	
	switch (interface->linkType) {
	case LCD_ParallelLink:
		result = __lcd_parallelDataIn(interface, LCD_Instruction);
		break;
	
	case LCD_SerialLink:
		result = __lcd_serialDataIn(interface, LCD_Instruction);
		break;
	}
	
	return result;
}

unsigned char lcd_readData(LCD_Interface *interface) {
	unsigned char result = 0;
	
	switch (interface->linkType) {
	case LCD_ParallelLink:
		result = __lcd_parallelDataIn(interface, LCD_Data);
		break;
	
	case LCD_SerialLink:
		result = __lcd_serialDataIn(interface, LCD_Data);
		break;
	}
	
	return result;
}
