/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "stc8f.h"
#include "delay.h"
#include "pca-stc8.h"
#include <math.h>

// Uncomment when debugging, as well as console_initialise() in main().
//#include "console-stc8.h"

/*
 * == DEFINITIONS ======================================================
 */

/*
 * -- PCA CLOCK SOURCE -------------------------------------------------
 */

#define TIMER0_FREQ 51200UL
#define TIMER0_PERIOD_NS 19531UL

/*
 * -- SERVO ------------------------------------------------------------
 */

#define SERVO_RIGHTMOST_POSITION 51
#define SERVO_MIDDLE_POSITION 77
#define SERVO_LEFTMOST_POSITION 103

#define SERVO_FULL_SWEEP_ANGLE 90
#define SERVO_FULL_SWEEP_STEPS (SERVO_LEFTMOST_POSITION - SERVO_RIGHTMOST_POSITION)

// The PWM channel is CCP1 on P1.6.
#define SERVO_PWM_CHANNEL     PCA_CHANNEL1

#define SERVO_FULL_SWEEP_DELAY 150
#define SERVO_SINGLE_STEP_DELAY 3

/*
 * -- ULTRASONIC RANGING SENSOR ----------------------------------------
 */

#include "us-ranger.h"

#define USRANGER_MAX_RANGE 400
#define USRANGER_CAPTURE_CHANNEL PCA_CHANNEL0

/*
 * PIN ASSIGNMENTS:
 * 
 * The trigger input of the sensor is connected to P2.4.
 * The echo output of the sensor is connected to CCP0 on P1.7.
 */

USRanger_Sensor usRangerSensor = {
	.triggerInput = GPIO_PIN_CONFIG(GPIO_PORT2, GPIO_PIN4, GPIO_BIDIRECTIONAL),
	.echoOutput   = PCA_P1_CHANNEL0_PIN,
};

/*
 * -- TEMPERATURE SENSOR -----------------------------------------------
 */

#include "DS18B20.h"

/*
 * PIN ASSIGNMENT:
 * 
 * The 1-Wire pin of the temperature sensor is connected to P2.3.
 */
DS18B20_Sensor ds18b20Sensor = {
	.oneWirePin = GPIO_PIN_CONFIG(GPIO_PORT2, GPIO_PIN3, GPIO_OPEN_DRAIN),
};

/*
 * -- LCD DISPLAY ------------------------------------------------------
 * 
 * The LCD display module is a 12864 with an ST7920 controller.
 * For maximum speed, and because we have plenty of GPIO pins,
 * we'll use its parallel interface in 8-bit mode.
 */

#include "lcd-interface.h"
#include "lcd-graphics.h"

/*
 * PIN ASSIGNMENTS:
 * 
 * The 8-bit data bus is connected to P0.
 * The E pin is connected to P2.7.
 * The RW pin is connected to P2.6.
 * The RS pin is connected to P2.5.
 */
LCD_ParallelLinkConfig lcdLinkConfig = {
	.dataBus  = GPIO_PORT_CONFIG(GPIO_PORT0, GPIO_BIDIRECTIONAL),
	.enOutput = GPIO_PIN_CONFIG(GPIO_PORT2, GPIO_PIN7, GPIO_BIDIRECTIONAL),
	.rwOutput = GPIO_PIN_CONFIG(GPIO_PORT2, GPIO_PIN6, GPIO_BIDIRECTIONAL),
	.rsOutput = GPIO_PIN_CONFIG(GPIO_PORT2, GPIO_PIN5, GPIO_BIDIRECTIONAL),
};

/*
 * LCD device definition.
 */
LCD_GRAPHICS_DEVICE(lcdDevice, LCD_ParallelLink, &lcdLinkConfig, 4, 16, 128, 64)

/*
 * -- PLOT SETTINGS ----------------------------------------------------
 */

// Rotate the plot by 90° (counter-clockwise).
#define PLOT_ORIENTATION 90

// Plot origin is bottom-middle of the screen.
#define PLOT_ORIGIN_X ((int) (lcdDevice.pixelWidth / 2))
#define PLOT_ORIGIN_Y ((int) lcdDevice.pixelHeight)

/*
 * -- SPLASH SCREEN ----------------------------------------------------
 */

#include "smiley.xbm"
#define SPLASH_SCREEN_DURATION 1000

/*
 * == IMPLEMENTATION ===================================================
 */

volatile unsigned long captureCount = 0;
volatile unsigned char capturing = 0;

/*
 * @see pca.h
 */
void pca_onCapture(PCA_Channel channel, unsigned long pulseLength) __using 1 {
	if (channel == USRANGER_CAPTURE_CHANNEL) {
		captureCount = pulseLength;
		capturing = 0;
	}
}

unsigned int measureDistance(unsigned int speedOfSound) {
	usRanger_triggerMeasurement(&usRangerSensor);
	
	// Measure roundtrip time ------------------------------------------
	captureCount = 0;
	capturing = 1;
	pca_startCapture(USRANGER_CAPTURE_CHANNEL, PCA_FALLING_EDGE, PCA_ONE_SHOT);
	while (capturing);
	
	unsigned long roundtrip = captureCount * TIMER0_PERIOD_NS / 1000UL;
	
	// Calculate distance in centimeters -------------------------------
	// 100cm / 1000000us = 10000UL.
	unsigned int distance = (roundtrip >> 1) * ((unsigned long) speedOfSound) / 10000UL;
	
	return distance;
}

void displayDataPoint(int position, unsigned int distance) {
	// Don't display anything if there's no obstacle :)
	if (distance <= USRANGER_MAX_RANGE) {
		// SDCC doesn't support double-precision floating point numbers.
		// Calculate polar coordinates.
		float angleDeg = ((position - SERVO_MIDDLE_POSITION) * ((float) SERVO_FULL_SWEEP_ANGLE) / SERVO_FULL_SWEEP_STEPS) + PLOT_ORIENTATION;
		float angle = angleDeg * PI / 180.0;
		float radius = distance * ((float) lcdDevice.pixelHeight) / USRANGER_MAX_RANGE;
		// Calculate rectangular offsets.
		int dx = floorf(radius * cosf(angle) + 0.5);
		int dy = floorf(radius * sinf(angle) + 0.5);
		// Move to desired origin.
		int x = PLOT_ORIGIN_X + dx;
		int y = PLOT_ORIGIN_Y - dy;	// The Y axis of the display goes downwards and we want to plot upwards.
		lcd_gfxPoint(&lcdDevice, x, y, 1);
	}
}

inline int measureAmbientTemperature() {
	return ds18b20_readTemperature(&ds18b20Sensor) / 16;
}

inline unsigned int calculateSpeedOfSound(int temperature) {
	// The following computation gives a very good approximation
	// of the speed of sound at any given temperature in Celsius.
	// Error is less than 0.3% over the -20°C to +45°C range.
	// See https://en.wikipedia.org/wiki/Speed_of_sound and the 
	// speed-of-sound.ods spreadsheet for more information.
	return (unsigned int) (temperature * 6 / 10 + 331);
}

void moveServoMeasureDistanceAndDisplayDataPoint(int position, unsigned int speedOfSound) {
	// Move servo ------------------------------------------------------
	pca_setPwmDutyCycle(SERVO_PWM_CHANNEL, position);
	delay1ms(SERVO_SINGLE_STEP_DELAY);
	
	displayDataPoint(position, measureDistance(speedOfSound));
}

unsigned char performScan(unsigned char leftToRight) {
	lcd_gfxClear(&lcdDevice);
	unsigned int speedOfSound = calculateSpeedOfSound(measureAmbientTemperature());
	
	if (leftToRight) {
		for (int position = SERVO_LEFTMOST_POSITION; position >= SERVO_RIGHTMOST_POSITION; position--) {
			moveServoMeasureDistanceAndDisplayDataPoint(position, speedOfSound);
		}
	} else {
		for (int position = SERVO_RIGHTMOST_POSITION; position <= SERVO_LEFTMOST_POSITION; position++) {
			moveServoMeasureDistanceAndDisplayDataPoint(position, speedOfSound);
		}
	}
	
	return !leftToRight;
}

void main() {
    // Initialise console when debugging -------------------------------
    //console_initialise(115200UL);
	
    // Initialise LCD module -------------------------------------------
    lcd_initialiseDevice(&lcdDevice);
    
    // Initialise ultrasonic ranging sensor ----------------------------
    usRanger_initialise(&usRangerSensor);
	
    // Initialise temperature sensor -----------------------------------
    ds18b20_initialise(&ds18b20Sensor, DS18B20_9bit);
    
    // Initialise PCA and start PWM ------------------------------------
	pca_initialise(PCA_TIMER0, PCA_FREE_RUNNING, PCA_INTERRUPT_ENABLE, PCA_P1);
	pca_startTimer0(TIMER0_FREQ);
	
	pca_startPwm(SERVO_PWM_CHANNEL, PCA_10BIT_PWM, SERVO_LEFTMOST_POSITION);
	
	// Wait for servo to reach leftmost position -----------------------
	delay1ms(SERVO_FULL_SWEEP_DELAY);
	
    // Enable interrupts -----------------------------------------------
	EA = 1;
	
    // Display splash screen :) ----------------------------------------
	lcd_gfxInitialiseDisplayMode(&lcdDevice);
	lcd_gfxXbmImage(&lcdDevice, smiley_width, smiley_height, smiley_bits, LCD_ALIGN_MIDDLE_CENTER, 0, 0);
	delay1ms(SPLASH_SCREEN_DURATION);
	
	// Main loop -------------------------------------------------------
	unsigned char leftToRight = 1;

    while (1) {
		leftToRight = performScan(leftToRight);
	}
}
